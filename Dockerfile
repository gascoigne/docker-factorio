FROM ubuntu:16.04
MAINTAINER Greg Taylor <gtaylor@gc-taylor.com>

RUN apt-get update && apt-get dist-upgrade -y && \
    apt install -y python3 xz-utils wget && apt-get clean

WORKDIR /opt

ARG factorio_version
ENV VERSION $factorio_version

COPY entrypoint.sh gen_config.py gen_map_settings.py factorio.crt /opt/
RUN wget -O /tmp/factorio_headless.tar.xz https://www.factorio.com/get-download/${VERSION}/headless/linux64

VOLUME /opt/factorio/saves /opt/factorio/mods

RUN tar -xJf /tmp/factorio_headless.tar.xz && \
    rm /tmp/factorio_headless.tar.xz

EXPOSE 34197/udp
EXPOSE 27015/tcp

CMD ["./entrypoint.sh"]
