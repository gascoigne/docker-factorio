import os
import json

CONFIGS = {
    'peaceful_mode': {
        'evar': 'FACTORIO_PEACEFUL_MODE',
        'default': false,
    }
}


def get_and_validate_vals():
    conf = {}
    for conf_name, conf_details in CONFIGS.items():
        evar = conf_details['evar']
        default = conf_details['default']
        conf_type = conf_details.get('type', 'str')
        conf_value = os.environ.get(evar, default)

        if conf_type == 'bool' and not isinstance(conf_value, bool):
            conf_value = conf_value.strip().lower() == 'true'
        elif conf_type == 'list':
            conf_value = conf_value.split()

        if 'nest_under' in conf_details:
            nest_under = conf_details['nest_under']
            if nest_under not in conf:
                conf[nest_under] = {}
            conf[nest_under][conf_name] = conf_value
        else:
            conf[conf_name] = conf_value

    return conf


def dump_config_json(conf):
    print(json.dumps(conf, indent=2))


def main():
    conf = get_and_validate_vals()
    dump_config_json(conf)


if __name__ == '__main__':
    main()
